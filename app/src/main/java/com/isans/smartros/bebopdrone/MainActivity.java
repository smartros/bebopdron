package com.isans.smartros.bebopdrone;


import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;


import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DICTIONARY_KEY_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_ERROR_ENUM;
import com.parrot.arsdk.arcontroller.ARControllerArgumentDictionary;
import com.parrot.arsdk.arcontroller.ARControllerDictionary;
import com.parrot.arsdk.arcontroller.ARControllerException;
import com.parrot.arsdk.arcontroller.ARFrame;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDevice;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceNetService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryException;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiver;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiverDelegate;
import com.parrot.arsdk.arsal.ARSALPrint;
import com.parrot.arsdk.arsal.ARSAL_PRINT_LEVEL_ENUM;
import org.ros.android.smartRosActivity;
import com.parrot.arsdk.arcontroller.ARDeviceController;
import com.parrot.arsdk.arcontroller.ARDeviceControllerListener;
import com.parrot.arsdk.arcontroller.ARDeviceControllerStreamListener;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
//wifi
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.ScanResult;

public class MainActivity extends smartRosActivity
        implements ARDiscoveryServicesDevicesListUpdatedReceiverDelegate
{
    private static String TAG = "MainActivity";
    public MainActivity() {super("bebopdrone Node");}
    private String                                _busyId;
    private List<ARDiscoveryDeviceService>       deviceList;
    private ARDiscoveryService                   ardiscoveryService;
    private boolean                              ardiscoveryServiceBound = false;
    private ServiceConnection                    ardiscoveryServiceConnection;
    private BroadcastReceiver                    ardiscoveryServicesDevicesListUpdatedReceiver;

    public IBinder                               discoveryServiceBinder;
    public ARDeviceController                    deviceController;
    public ARDiscoveryDeviceService              service;
    public ARDiscoveryDevice                     device;
    private String                               ConnectedWifi = " ";

    //wifi
    private WifiManager                          wifiMgr;
    private List                                 apList;
    private Context                              context;
    WifiConfiguration wfc                      = new WifiConfiguration();
    boolean bWifiConnect                       = false;
    private int nCount                         = 0;
    private ConnectedWifideviceThread            cwThread  ;
    private Timer mTimer                       = null;


    static
    {
        try
        {
            System.loadLibrary("arsal");
            System.loadLibrary("arsal_android");
            System.loadLibrary("arnetworkal");
            System.loadLibrary("arnetworkal_android");
            System.loadLibrary("arnetwork");
            System.loadLibrary("arnetwork_android");
            System.loadLibrary("arcommands");
            System.loadLibrary("arcommands_android");
            System.loadLibrary("json");
            System.loadLibrary("ardiscovery");
            System.loadLibrary("ardiscovery_android");
            System.loadLibrary("arstream");
            System.loadLibrary("arstream_android");
            System.loadLibrary("arcontroller");
            System.loadLibrary("arcontroller_android");

            ARSALPrint.setMinimumLogLevel(ARSAL_PRINT_LEVEL_ENUM.ARSAL_PRINT_INFO);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Oops (LoadLibrary)", e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //wifi
        context = getApplicationContext();
        wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);


        wfc = new WifiConfiguration();
        wfc.SSID = "".concat("Bebop");
        wfc.status = WifiConfiguration.Status.DISABLED;
        wfc.priority = 40;
        //
        moveTaskToBack(true);
        deviceList = new ArrayList<>();

    }
    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        long endTime = System.currentTimeMillis();
        long duration;
        String[] parts = s.split("/");

        switch (parts[1]) {
            case "start_all":
                break;
            case "reset_all":
                if(deviceController != null) {
                    deviceController.getFeatureARDrone3().setPilotingPCMDPitch((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDGaz((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDYaw((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDRoll((byte) 0);
                }
                if ((deviceController != null) && (deviceController.getFeatureARDrone3() != null))
                {
                    //send landing
                    ARCONTROLLER_ERROR_ENUM error = deviceController.getFeatureARDrone3().sendPilotingLanding();
                }
                break;
            case "bebop_until_connect":
                _busyId = parts[2];
                if(mTimer != null ) {
                    mTimer.cancel();
                }
                Log.d(TAG, "bebop until");

                //wifi scan
                nCount++;
                Log.d(TAG, "nCount  = " + nCount);
                wifiMgr.startScan();
                IntentFilter  filter = new IntentFilter();
                filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
                registerReceiver(wifiReceiver, filter);
                Log.d(TAG, "Wifi scan");

                if(!wifiMgr.isWifiEnabled())
                    if(wifiMgr.getWifiState() != WifiManager.WIFI_STATE_ENABLED){
                        wifiMgr.setWifiEnabled(true);
                    }
                mTimer = new Timer();
                MyTask task = new MyTask();
                mTimer.schedule(task, 2000, 2000);

                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initBroadcastReceiver();
                        initServiceConnection();
                        Log.d(TAG,"1, Delayed");
                    }
                }, 5000);

                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cwThread = new ConnectedWifideviceThread();
                        cwThread.start();
                        Log.d(TAG, "2, Delayed");
                    }
                }, 5000);

                SystemClock.sleep(15000);
                if(deviceList.size() > 0)
                    delayHandler.sendEmptyMessage(100);

                break;
            case "bebop_start":

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), " bebopStart", Toast.LENGTH_SHORT).show();
                    }
                });

                if(deviceList.size() > 0) {
                    try {
                        device = new ARDiscoveryDevice();
                        ARDiscoveryDeviceService mService = deviceList.get(0);
                        ARDiscoveryDeviceNetService netDeviceService = (ARDiscoveryDeviceNetService) mService.getDevice();

                        device.initWifi(ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_ARDRONE, netDeviceService.getName(), netDeviceService.getIp(), netDeviceService.getPort());
                    } catch (ARDiscoveryException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error: " + e.getError());
                    }
                    service = deviceList.get(0);
                    if(service != null) {
                        if(deviceController != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), " On_DeviceController", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                        }

                        if(device != null) {
                            try {
                                deviceController = new ARDeviceController(device);
                                deviceController.addListener(new ARDeviceControllerListener() {
                                    @Override
                                    public void onStateChanged (ARDeviceController deviceController, ARCONTROLLER_DEVICE_STATE_ENUM newState, ARCONTROLLER_ERROR_ENUM error)
                                    {
                                        Log.i(TAG, "onStateChanged ... newState:" + newState+" error: "+ error );
                                        switch (newState)
                                        {
                                            case ARCONTROLLER_DEVICE_STATE_RUNNING:
                                                //The deviceController is started
                                                Log.i(TAG, "ARCONTROLLER_DEVICE_STATE_RUNNING .....");
                                                deviceController.getFeatureARDrone3().sendMediaStreamingVideoEnable((byte)1);
                                                break;
                                            case ARCONTROLLER_DEVICE_STATE_STOPPED:
                                                //The deviceController is stoped
                                                Log.i(TAG, "ARCONTROLLER_DEVICE_STATE_STOPPED .....");
                                                deviceController.dispose();
                                                deviceController = null;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    @Override
                                    public void onCommandReceived(ARDeviceController deviceController, ARCONTROLLER_DICTIONARY_KEY_ENUM commandKey, ARControllerDictionary elementDictionary)
                                    {
                                        if (elementDictionary != null)
                                        {
                                            if (commandKey == ARCONTROLLER_DICTIONARY_KEY_ENUM.ARCONTROLLER_DICTIONARY_KEY_COMMON_COMMONSTATE_BATTERYSTATECHANGED)
                                            {
                                                ARControllerArgumentDictionary<Object> args = elementDictionary.get(ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY);
                                                if (args != null)
                                                {
                                                    Integer batValue = (Integer) args.get("arcontroller_dictionary_key_common_commonstate_batterystatechanged_percent");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Log.e(TAG, "elementDictionary is null");
                                        }
                                    }
                                });
                                deviceController.addStreamListener(new ARDeviceControllerStreamListener() {
                                    @Override
                                    public void onFrameReceived(ARDeviceController arDeviceController, ARFrame arFrame) {
                                    }

                                    @Override
                                    public void onFrameTimeout(ARDeviceController arDeviceController) {
                                    }
                                });
                            } catch (ARControllerException e) {
                                e.printStackTrace();
                            }
                        }

                        startDeviceController();
                    }
                }
                break;
            case "bebop_stop":
                if(deviceController != null) {
                    deviceController.getFeatureARDrone3().setPilotingPCMDPitch((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDGaz((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDYaw((byte) 0);
                    deviceController.getFeatureARDrone3().setPilotingPCMDRoll((byte) 0);
                }
                break;
            case "bebop_landing":
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), " landing" ,Toast.LENGTH_SHORT).show();
                    }
                });
                if ((deviceController != null) && (deviceController.getFeatureARDrone3() != null))
                {
                    //send landing
                    ARCONTROLLER_ERROR_ENUM error = deviceController.getFeatureARDrone3().sendPilotingLanding();
                }
                break;
            case "bebop_takeoff":
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), " takeoff" ,Toast.LENGTH_SHORT).show();
                    }
                });
                if ((deviceController != null) && (deviceController.getFeatureARDrone3() != null))
                {
                    //send takeOff
                    ARCONTROLLER_ERROR_ENUM error = deviceController.getFeatureARDrone3().sendPilotingTakeOff();
                }
                break;
            case "bebop_emergency":
                if ((deviceController != null) && (deviceController.getFeatureARDrone3() != null))
                {
                    ARCONTROLLER_ERROR_ENUM error = deviceController.getFeatureARDrone3().sendPilotingEmergency();
                }
                break;
            case "bebop_roll_control":
                if(deviceController != null) {
                    if (parts[2].equals("forward")) {
                        deviceController.getFeatureARDrone3().setPilotingPCMDPitch((byte) 50);
                        deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 1);
                    } else if (parts[2].equals("back")) {
                        deviceController.getFeatureARDrone3().setPilotingPCMDPitch((byte) -50);
                        deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 1);
                    } else if (parts[2].equals("left")) {
                        deviceController.getFeatureARDrone3().setPilotingPCMDRoll((byte) -50);
                        deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 1);
                    } else if (parts[2].equals("right")) {
                        deviceController.getFeatureARDrone3().setPilotingPCMDRoll((byte) 50);
                        deviceController.getFeatureARDrone3().setPilotingPCMDFlag((byte) 1);
                    }
                }
                break;
            case "bebop_yaw_control":
                if(deviceController != null) {
                    if(parts[2].equals("up"))
                    {
                        deviceController.getFeatureARDrone3().setPilotingPCMDGaz((byte) 50);
                    }
                    else if(parts[2].equals("down"))
                    {
                        deviceController.getFeatureARDrone3().setPilotingPCMDGaz((byte)-50);
                    }
                    else if(parts[2].equals("left"))
                    {
                        deviceController.getFeatureARDrone3().setPilotingPCMDYaw((byte)-50);
                    }
                    else if(parts[2].equals("right"))
                    {
                        deviceController.getFeatureARDrone3().setPilotingPCMDYaw((byte) 50);
                    }
                }
                break;
        }

    }


    @Override
    public void onBackPressed()
    {
        stopDeviceController();
    }
    @Override
    public void onResume()
    {
        Log.d(TAG, "onResume ...");
        super.onResume();
    }
    @Override
    public void onStop()
    {
        Log.d(TAG,"onStop");
        super.onStop();
    }

    @Override
    public void onRestart()
    {
        Log.d(TAG , "onRestart");
        super.onRestart();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceivers();
        bWifiConnect = false;
        closeServices();
        stopDeviceController();
        unregisterReceiver(wifiReceiver);
        wifiMgr.disconnect();


        if(cwThread.isAlive())
            cwThread.interrupt();

        if(mTimer != null)
            mTimer.cancel();

        nCount = 0;


    }
    @Override
    public void onServicesDevicesListUpdated()
    {
        Log.i(TAG, "onServicesDevicesListUpdated ...");
        List<ARDiscoveryDeviceService> list;

        if (ardiscoveryService != null)
        {
            sendRespMessage("bebop_get_device 1\n");
            sendRespMessage("bebop_is_started false\n");
            list = ardiscoveryService.getDeviceServicesArray();
            deviceList = new ArrayList<ARDiscoveryDeviceService> ();



            if(list != null)
            {
                for (ARDiscoveryDeviceService service : list)
                {
                    sendRespMessage("bebop_get_device " + service + "\n");
                    Log.e(TAG, "service :  " + service + " name = " + service.getName());
                    ARDISCOVERY_PRODUCT_ENUM product = ARDiscoveryService.getProductFromProductID(service.getProductID());
                    Log.e(TAG, "product :  "+ product);
                    // only display Bebop drones
                    if (ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_ARDRONE.equals(product))
                    {
                        deviceList.add(service);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), " deviceList add", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

        }

    }
    private class MyTask extends TimerTask{
        int nChkTime = 0;
        @Override
        public void run(){

            //wifi connect
            if(!ConnectedWifi.equals(wfc.SSID)) {

                int mCount = 0;

                String[] TestText = ConnectedWifi.split("(?!^)\\b|\\B");
                String[] TestText2 = wfc.SSID.split("(?!^)\\b|\\B");


                for (int i = 0; i < 4; i++) {
                    if (TestText[i].equals(TestText2[i]) == true) {
                        mCount++;
                    } else {
                        break;
                    }
                    if (mCount == 3) {
                        connectWifi(ConnectedWifi);
                        Log.d(TAG, "wifi connect");
                    }
                }

                if (bWifiConnect) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "연결 했습니다..\n", Toast.LENGTH_SHORT).show();
                        }
                    });
                    mTimer.cancel();
                }
            }
            else
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "연결.\n", Toast.LENGTH_SHORT).show();
                    }
                });
                mTimer.cancel();
            }


        }
    }
    private Handler delayHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(msg.what == 100)
            {
                sendRespMessage(String.format("_busy %s\n", _busyId));
                Log.d(TAG, "_busy" + _busyId);
                if(mTimer != null)
                    mTimer.cancel();
                if(cwThread.isAlive()) {
                    cwThread.interrupt();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "interrupt thread", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

        };
    };


    private class ConnectedWifideviceThread extends Thread {
        @Override
        public void run() {
            onServicesDevicesListUpdated();
            registerReceivers();
            initServices();


        }

    }
    private void initBroadcastReceiver()
    {
        ardiscoveryServicesDevicesListUpdatedReceiver = new ARDiscoveryServicesDevicesListUpdatedReceiver(this);
    }


    private void initServiceConnection()
    {
        ardiscoveryServiceConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service)
            {
                discoveryServiceBinder = service;
                ardiscoveryService = ((ARDiscoveryService.LocalBinder) service).getService();
                ardiscoveryServiceBound = true;

                ardiscoveryService.start();
                Log.v(TAG, "onServiceConnected 실행됨");
            }

            @Override
            public void onServiceDisconnected(ComponentName name)
            {
                ardiscoveryService = null;
                ardiscoveryServiceBound = false;
                Log.v(TAG, "onServiceDisconnected 실행됨");
            }
        };
    }

    private void registerReceivers()
    {
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastMgr.registerReceiver(ardiscoveryServicesDevicesListUpdatedReceiver, new IntentFilter(ARDiscoveryService.kARDiscoveryServiceNotificationServicesDevicesListUpdated));
        Log.i(TAG, "registerReceivers Start");
    }

    private void unregisterReceivers()
    {
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastMgr.unregisterReceiver(ardiscoveryServicesDevicesListUpdatedReceiver);
    }


    private void initServices()
    {
        Log.i(TAG, "initServices");
        if (discoveryServiceBinder == null)
        {
            Intent i = new Intent(getApplicationContext(), ARDiscoveryService.class);
            getApplicationContext().bindService(i, ardiscoveryServiceConnection, Context.BIND_AUTO_CREATE);
        }
        else
        {
            ardiscoveryService = ((ARDiscoveryService.LocalBinder) discoveryServiceBinder).getService();
            ardiscoveryServiceBound = true;
            ardiscoveryService.start();
        }
    }

    private void closeServices()
    {
        Log.d(TAG, "closeServices ...");

        if (ardiscoveryServiceBound)
        {
            new Thread(new Runnable() {
                @Override
                public void run()
                {
                    ardiscoveryService.stop();
                    getApplicationContext().unbindService(ardiscoveryServiceConnection);
                    ardiscoveryServiceBound = false;
                    discoveryServiceBinder = null;
                    ardiscoveryService = null;
                }
            }).start();
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    private void startDeviceController()
    {
        if (deviceController != null)
        {
            ARCONTROLLER_ERROR_ENUM error = deviceController.start();
            Log.i(TAG, "deviceController Start");
            sendRespMessage("bebop_is_started true\n");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "bebopDrone is connected", Toast.LENGTH_SHORT).show();

                }
            });
            if (error != ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK)
            {
                finish();
            }

        }
    }

    private void stopDeviceController()
    {
        if (deviceController != null)
        {
            ARCONTROLLER_ERROR_ENUM error = deviceController.stop();

            if (error != ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK)
            {
                finish();
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "bebopDrone is disconnected", Toast.LENGTH_SHORT).show();
                    sendRespMessage("bebop_is_started false\n");
                }
            });



        }
    }
    //wifi
    private BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                searchWifi();
            }
        }

    };

    public void searchWifi() {
        apList = wifiMgr.getScanResults();
        if (wifiMgr.getScanResults() != null) {
            int size = apList.size();
            for (int i = 0; i < size; i++) {
                Log.i(TAG, "wifi : " + apList.get(i));
                String [] text = ((ScanResult) apList.get(i)).SSID.split("(?!^)\\b|\\B");
                String [] text2 = wfc.SSID.split("(?!^)\\b|\\B");
                int nCount = 0;
                for(int j = 0 ; j< 4; j++) {
                    if(text[j].equals(text2[j]) == true ) {
                        nCount++;
                    }
                    else {
                        break;
                    }
                    if(nCount > 2) {
                        ConnectedWifi = ((ScanResult) apList.get(i)).SSID;
                    }
                }
            }
        }
    }

    public boolean connectWifi(String ssid) { //open Wifi

        WifiConfiguration wfc = new WifiConfiguration();

        wfc.SSID = "\"".concat(ssid).concat("\"");
        wfc.status = WifiConfiguration.Status.DISABLED;
        wfc.priority = 40;

        wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wfc.allowedAuthAlgorithms.clear();
        wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

        boolean connection = false;
        WifiManager wfMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configList = wfMgr.getConfiguredNetworks();

        int networkId = -1;
        for(WifiConfiguration wifiConfig : configList) {
            if (wifiConfig.SSID.equals(wfc.SSID)) {
                networkId = wifiConfig.networkId;
                connection = true;
                break;
            }
        }

        if(!connection) {
            networkId = wfMgr.addNetwork(wfc);
            wfMgr.saveConfiguration();

        }
        if (networkId != -1) {
            connection = wfMgr.enableNetwork(networkId, true);
            bWifiConnect = true;
        }

        return connection;
    }



}